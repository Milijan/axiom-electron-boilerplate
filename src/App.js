import React, { Component } from 'react'

// Styles
import './css/oswald.css'
import './css/open-sans.css'
import './css/pure-min.css'
import './App.css'

const IPFS = require('ipfs')
const node = new IPFS()
node.on('ready', () => {
  // Your node is now ready to use \o/
  console.log("IPFS working!!")

  // stopping a node
  node.stop(() => {
    // node is now 'offline'
  })
})

class App extends Component {
  render() {
    return (
      <div className="App">
        {this.props.children}
      </div>
    );
  }
}

export default App
